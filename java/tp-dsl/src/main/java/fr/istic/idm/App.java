package fr.istic.idm;

import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.Result;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Hello world!
 *
 */
public class App 
{
    public static Connection connect() {

        Connection cnx = null;
        try {
            Class.forName("org.sqlite.JDBC");
            cnx = DriverManager.getConnection("jdbc:sqlite:../../data.db");
        } catch ( Exception e ) {
            e.printStackTrace();
            System.exit(0);
        }
        System.out.println("Opened database successfully");

        return cnx;

    }

    public static void disconnect(Connection cnx) {

        try {
            cnx.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void main( String[] args )
    {

        Connection cnx = connect();

        DSLContext create = DSL.using(cnx, SQLDialect.SQLITE);

        /*
            SELECT * FROM person
            WHERE firstname != 'ludo'
            ORDER BY lastname DESC
        */

        Result<Record> result = create.select()
                                      .from("person")
                                      .where("firstname != 'ludo'")
                                      .orderBy(DSL.fieldByName("lastname").desc())
                                      .fetch();

        System.out.println("Query 1");
        display(result);

        /*
            SELECT * FROM person
            WHERE gender_code = 'F'
         */

        result = create.select()
                .from("person")
                .where("gender_code = 'F'")
                .fetch();

        System.out.println("Query 2");
        display(result);

        /*
            SELECT * FROM person
            WHERE lastname = 'foobar'
         */

        result = create.select()
                .from("person")
                .where("lastname = 'foobar'")
                .fetch();

        System.out.println("Query 3");
        display(result);

        disconnect(cnx);

    }

    private static void display(Result<Record> _result) {

        for (Record r : _result) {

            String firstName = (String) r.getValue("firstname");
            String lastName = (String) r.getValue("lastname");

            System.out.println(firstName + " " + lastName);
        }

        System.out.println("");

    }
}
