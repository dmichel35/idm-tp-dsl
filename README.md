# TP DSL #

This project is composed of a SQLite database and 2 mini-apps : 

* one written in Java using _Maven_, a _JDBC driver_ and _Jooq_
* one written in PHP using a library called _FluentPDO_

### How to execute the queries ###

For the java project, you need to execute the jar file located in java/tp-dsl/target :

```
#!shell
java -jar target/tp-dsl-1.0-jar-with-dependencies.jar

```

And for the PHP project, you need to put the PHP files in an Apache Web Server and open the page _index.php_ in a browser.

### SQL, Java & PHP ###

Consider the following SQL query :


```
#!SQL

SELECT * FROM person WHERE lastname = 'foobar'
```

Then, its implementation with Java and Jooq will be :


```
#!java

result = create.select()
                .from("person")
                .where("lastname = 'foobar'")
                .fetch();
```

And in PHP with FluentPDO, it will be : 


```
#!php

 $query = $fpdo->from('person')
 				->where('lastname', 'foobar');
```

