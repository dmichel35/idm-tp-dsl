CREATE TABLE person
 ( 
    num               INT              NOT NULL  , 
    firstname         VARCHAR(20)          NULL  , 
    lastname          VARCHAR(30)          NULL  ,         
    gender_code       VARCHAR(1)           NULL 
 );

INSERT INTO person VALUES(1, 'jean', 'dupont', 'M');
INSERT INTO person VALUES(2, 'chuck', 'norris', 'M');
INSERT INTO person VALUES(3, 'ludo', 'routier', 'M');
INSERT INTO person VALUES(4, 'pauline', 'foobar', 'F');
INSERT INTO person VALUES(5, 'paul', 'foobar', 'M');