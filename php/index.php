<?php

include "lib/FluentPDO/FluentPDO.php";

$pdo = new PDO("sqlite:../data.db");
$fpdo = new FluentPDO($pdo);

/*
	SELECT * FROM person 
	WHERE firstname != 'ludo'
	ORDER BY lastname DESC
*/

$query = $fpdo->from('person')
			->where("firstname != 'ludo'")
			->orderBy("lastname DESC");

echo "Query 1 <br/>";
display($query);

/*
    SELECT * FROM person
    WHERE gender_code = 'F'
 */

 $query = $fpdo->from('person')
 				->where('gender_code', 'F');

echo "Query 2 <br/>";
display($query);

/*
    SELECT * FROM person
    WHERE lastname = 'foobar'
 */

 $query = $fpdo->from('person')
 				->where('lastname', 'foobar');

 echo "Query 3 <br/>";
 display($query);

 function display($query) {

 	foreach ($query as $row) {
		echo $row['firstname'].' '.$row['lastname'];
		echo '<br/>';
	}
	echo '<br/>';

 }

?>